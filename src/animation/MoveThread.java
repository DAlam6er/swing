
package animation;

public class MoveThread implements Runnable
{
    
    private final Thread self;
    private volatile boolean canWork;
    private int delay;
    private final JBoxFrame frame;

    public MoveThread(JBoxFrame frame, int delay)
    {
        this.frame = frame;
        this.delay = delay;
        self = new Thread(this);
        canWork = true;
    }

    public int getDelay()
    {
        return delay;
    }

    public void setDelay(int delay)
    {
        this.delay = delay;
    }
    
    public void start()
    {
        self.start();
    }
    
    public void stop()
    {
        canWork = false;
    }
    
    @Override
    public void run()
    {
        while(canWork) {
            frame.moveBox();
            try {
                Thread.sleep(delay); // пример "математики"
            } catch (InterruptedException ex) {
                break;
            }
        }
    }
    
}
